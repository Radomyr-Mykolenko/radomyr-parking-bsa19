﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace example_about_events
{
    public class WalletEventArgs
    {
        // Wallet action message
        public string ActionMessage { get; }

        // Amount of money on which wallet balance changed 
        public int Amount { get; }

        public WalletEventArgs(string actionMessage, int amount)
        {
            ActionMessage = actionMessage;
            Amount = amount;
        }
    }
    public struct Wallet
    {
        // Define an delegate
        public delegate void WalletStateHandler(object sender, WalletEventArgs e);

        // Events, that fires when wallet balance is toped up
        public event WalletStateHandler ToppedUp;

        // Events, that fires when somebody withdraw money form a wallet balance
        public event WalletStateHandler Withdrawn;

        // Private field that stores amount of money in wallet
        private int _balance;

        public Wallet(int amount)
        {
            _balance = amount;
            ToppedUp = null;
            Withdrawn = null;
        }

        public void TopUp(int amount)
        {
            _balance += amount;
            var args = new WalletEventArgs($"The wallet was topped up on {amount} of money", amount);
            ToppedUp?.Invoke(this, args); // null condition operator
        }

        public void Withdraw(int amount)
        {
            if (_balance < amount)
            {
                var args = new WalletEventArgs($"Not enough money on wallet's balance", amount);
                Withdrawn?.Invoke(this, args);
            }
            else
            {
                _balance -= amount;
                var args = new WalletEventArgs($"{amount} of money was withdrawned from the wallet", amount);
                Withdrawn?.Invoke(this, args);
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var wallet = new Wallet(100);

            // Add event handlers
            wallet.ToppedUp += Log_TopUp;
            wallet.Withdrawn += delegate (object sender, WalletEventArgs e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Withdrawn: {e.Amount}");
                Console.WriteLine(e.ActionMessage);
            };

            wallet.Withdraw(90);

            wallet.Withdraw(20);

            wallet.TopUp(50);

            // Remove Top Up event handler
            wallet.ToppedUp -= Log_TopUp;

            wallet.TopUp(100);
            Console.ReadKey();
        }

        private static void Log_TopUp(object sender, WalletEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Top up amount: {e.Amount}");
            Console.WriteLine(e.ActionMessage);

        }

    }
}
