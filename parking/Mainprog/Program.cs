﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Timers;


namespace Mainprog
{
    static class Parking_Settings // статичний клас налаштувань парковки
    {
        static public float initial_balance = 0; // початковий баланс парковки
        static public int parking_capacity = 10; // максимальна місткість парковки
        //періодичність,  з якою будуть зніматись кошти, за замовчуванням це 5 секунд

            //тарифи за паркування
        public const float parking_price_for_smallcar = 2; //легкове авто
        public const float parking_price_for_lorry = 5;        //вантажне авто
        public const float parking_price_for_bus = 3.5f;       //автобус
        public const float parking_price_for_bike = 1;         //мотоцикл

        public const float penalty = 2.5f;         //коефіціент штрафу, який накладається на Транспортний засіб, якщо на його рахунку недостатньо коштів для оплати стоянки. 
                                                   //Допоки Транспортний засіб не виплатить гроші Парковці, з цього рахунку списуватимуться кошти з урахуванням цього коефіціенту
        public const float initial_vehicle_balance = 200;

        public const int time_for_parking = 8000; //параметр часу N
        
    }
   
    class Vehicle
    {
        public string vehicle_number;
        public string type_of_vehicle;      //lorry, bus, smallcar, bike
        public float vehicle_balance; //рахунок/баланс грошей на транспортному засобі       
        public float penalty;
        public enum VehicleType
        {
            smallcar,
            lorry,
            bus,
            bike
        }

        public float parking_price;         //сюди буде записуватись вартість паркування Транспортного засобу

        public Vehicle(string number, VehicleType type)
        {
            

            vehicle_number = number;
            type_of_vehicle = Convert.ToString(type);
            parking_price = 0; //компілятор чомусь попросив спочатку хоч якось ініціалізувати це поле
            switch (type)
            {
                case VehicleType.smallcar:                    
                    parking_price = Parking_Settings.parking_price_for_smallcar;
                    
                    break;                   
                case VehicleType.lorry:
                    parking_price = Parking_Settings.parking_price_for_lorry;
                    
                    break;
                case VehicleType.bus:
                    parking_price = Parking_Settings.parking_price_for_bus;
                    
                    break;
                case VehicleType.bike:
                    parking_price = Parking_Settings.parking_price_for_bike;
                    
                    break;
                default:
                    Console.WriteLine($"Не передбачений даною програмою тип авто {type}");
                    break;

            }
            vehicle_balance = Parking_Settings.initial_vehicle_balance;
           
            ToppedUp = null;
            Withdrawn = null;
            SetTimer(Parking_Settings.time_for_parking);           
        }

        

        public void Display_Info_about_vehicle()
        {
            Console.WriteLine($"  Номер ТЗ: {vehicle_number}, тип ТЗ: {type_of_vehicle}, \n плата за паркування для нього становить: {parking_price}, баланс рахунку для оплати паркування: {vehicle_balance}");
            Console.WriteLine();

        }

        //визначення делегату
        public delegate void BalanceVehicleStateHandler(object sender, BalanceVehicleEventArgs e);
        //події, які відбуваються, коли поповнюється рахунок транспортного засобу
        public event BalanceVehicleStateHandler ToppedUp;

        public event BalanceVehicleStateHandler Withdrawn;

        public void TopUp(float amount)
        {
            vehicle_balance += amount;
            var args = new BalanceVehicleEventArgs($"  Баланс транспортного засобу з номером {vehicle_number} був поповнений на {amount} грошей", amount);
            ToppedUp?.Invoke(this, args);
        }

        public void Withdraw(float amount)
        {
            if (vehicle_balance <= 0)
            {
                penalty = Parking_Settings.penalty;
                vehicle_balance -= (amount*penalty);  //застосовуємо штраф за мінусовий баланс
            }
            else
            {
                penalty = 1;
                vehicle_balance -= amount*penalty;                  
            }
        }

        private static Timer aTimer;
      
        public void SetTimer(int N) //таймер, який щохвилини записує в файл
        {
            // Create a timer with a two second interval.
            aTimer = new System.Timers.Timer(N);
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += OnTimedEvent;
           
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        public void OnTimedEvent(Object source, ElapsedEventArgs e)                  //тут власне і відбуваються транзакції
        {
            Withdraw(parking_price);
            //string timenow = Convert.ToString(DateTime.Now);
            //Parking.Transactions[Convert.ToString(timenow)] = new PaymentDetails() { vehicleId = vehicle_number, amountspent = Parking_Settings.parking_price_for_smallcar, vehicle_bal = vehicle_balance };
            Parking.Transactions[DateTime.Now] = new PaymentDetails() { vehicleId = vehicle_number, amountspent = parking_price, vehicletype = type_of_vehicle, vehicle_bal = vehicle_balance };
            Parking.parking_balance += parking_price;
            Parking.ParkingBalance[DateTime.Now] = parking_price*penalty;
        }

        public void Stoptimer()
        {
            aTimer.Stop();
            aTimer.Dispose();
        }
    }

    struct PaymentDetails
    {
        public string vehicleId;        // номер ТЗ
        public float amountspent;       // списана сума
        public string vehicletype;      // тип ТЗ
        public float vehicle_bal;       // баланс транспортного засобу
    }
    
    class Parking                               //Клас парковки
    {
        static public float parking_balance; //рахунок/баланс грошей на парковці
        public int vehicles_parked;     //запарковано транспортних засобів

        public Dictionary<string, Vehicle> Parked_vehicles = new Dictionary<string, Vehicle>(); //Словник, де зберігаємо запарковані автомобілі
        static public Dictionary<DateTime, float> ParkingBalance = new Dictionary<DateTime, float>(); // Журнал транзакцій парковки

        static public Dictionary<DateTime, PaymentDetails> Transactions = new Dictionary<DateTime, PaymentDetails>(); // Журнал транзакцій парковки
        static public void ShowTransactions()
        {

            foreach (KeyValuePair<DateTime, PaymentDetails> kvp in Transactions)
            {
                Console.WriteLine(" {0}  vehicle_Id  {1}    amount_spent  {2}   vehicle_type {3}   balance {4}",
                        kvp.Key.ToLongTimeString(), kvp.Value.vehicleId, kvp.Value.amountspent, kvp.Value.vehicletype, kvp.Value.vehicle_bal);
            }

        }

        static public void ShowMoneyReceived()
        {

            foreach (KeyValuePair<DateTime, float> kvp in ParkingBalance)
            {
                Console.WriteLine(" {0}  зароблено парковкою {1} грошей",
                        kvp.Key.ToLongTimeString(), kvp.Value);
            }

        }

        public void SetParkingBalance() //задаємо початковий баланс парковки
        {
            parking_balance = Parking_Settings.initial_balance;
        }

        public void ShowParkingBalance()
        {           
            Console.WriteLine($"  Поточний баланс парковки становить {parking_balance} грошей \n");
        }

        public void ShowQuantityVehiclesParked()
        {
            if (vehicles_parked == 0)
            {
                Console.WriteLine("  Парковка повнiстю вiльна, можна паркуватись \n");
                Console.WriteLine($"  Не забувайте, що найбiльше можна поставити {Parking_Settings.parking_capacity} транспортних засобiв \n");
            }
            else
            {
                Console.WriteLine($" На парковцi зайнято {vehicles_parked} мiсць");
                Console.WriteLine($" Доступно {Parking_Settings.parking_capacity - vehicles_parked} вiльних мiсць \n");
            }
        }

        public void ShowListOfVehiclesParked()
        {
            if (vehicles_parked != 0)
            {
                int i = 1;
                foreach (Vehicle c in Parked_vehicles.Values)
                {
                    Console.Write($"  {i} - ");
                    c.Display_Info_about_vehicle();
                    i++;
                }
            }
            else
            {
                Console.WriteLine("  Порожньо на парковцi");
            }
        }

        //public void ShowTransactions()
        //{

        //    foreach (KeyValuePair<string,PaymentDetails> kvp in Transactions)
        //    {
        //        Console.WriteLine("Key = {0}, vehicle_Id = {1}, amount_spent = {2}, balance = {3}",
        //                kvp.Key, kvp.Value.vehicleId, kvp.Value.amountspent, kvp.Value.vehicle_bal);             
        //    }
           
        //}

        //визначення делегату
        public delegate void BalanceParkingStateHandler(object sender, BalanceParkingEventArgs e);
        //події, які відбуваються, коли поповнюється рахунок парковки, ми лише поповнюємо рахунок парковки за рахунок списаних коштів із транспортних засобів
        public event BalanceParkingStateHandler ToppedUp;

        public event BalanceParkingStateHandler VehicleAdded;

        public event BalanceParkingStateHandler VehicleRemoved;

        public void TopUp(float amount)
        {
            parking_balance += amount;
            //var args = new BalanceParkingEventArgs($"Баланс парковки був поповнений на {amount} грошей", amount);
            //ToppedUp?.Invoke(this, args);
        }

        public void VehicleAdd()
        {
            if (vehicles_parked < Parking_Settings.parking_capacity)        //перевірка, чи не вийшли ми за ліміт парковки
            {
                vehicles_parked += 1;
                var args = new BalanceParkingEventArgs("  На парковку було додано транспортний засiб");
                VehicleAdded?.Invoke(this, args);
            }
            else
            {
                var args = new BalanceParkingEventArgs($"  Заборонено паркуватись, перевищено лiмiт в {Parking_Settings.parking_capacity} транспортних засобiв");
                VehicleRemoved?.Invoke(this, args);
            }
        }

        public void VehicleRemove()
        {
            if (vehicles_parked >= 1)
            {
                vehicles_parked -= 1;
                var args = new BalanceParkingEventArgs("  З парковки прибрали транспортний засiб");
                VehicleRemoved?.Invoke(this, args);
            }
            else
            {
                var args = new BalanceParkingEventArgs("  Вже всi прибранi транспортнi засоби");
                VehicleRemoved?.Invoke(this, args);
            }
        }

    }                               

    public class BalanceVehicleEventArgs            // клас аргументів подій для балансу транспортного засобу
    {
        public string ActionMessage { get; }

        public float Amount { get; }

        public BalanceVehicleEventArgs(string actionMessage, float amount)
        {
            ActionMessage = actionMessage;
            Amount = amount;
        }
    }

    public class BalanceParkingEventArgs            // клас аргументів подій для балансу парковки
    {
        public string ActionMessage { get; }

        public float Amount { get; }

        public int Quantity { get; }                //кількість авто на парковці

        public BalanceParkingEventArgs(string actionMessage, float amount)
        {
            ActionMessage = actionMessage;
            Amount = amount;
        }
        public BalanceParkingEventArgs(string actionMessage)
        {
            ActionMessage = actionMessage;
            Quantity = 1;
        }
    }

    class MainMenu
    {  
        string[] MainMenuContainer = new string[]
            {
                /*1*/        "Дiзнатися поточний баланс парковки",
                /*2*/        "Сума зароблених коштiв за останню хвилину",
                /*3*/        "Дiзнатися кiлькiсть вiльних/зайнятих мiсць на парковцi",
                /*4*/        "Вивести на екран всi Транзакцii Парковки за останню хвилину",
                /*5*/        "Вивести усю iсторiю Транзакцiй (зчитавши данi з файлу Transactions.log)",
                /*6*/        "Вивести на екран список усiх Транспортних засобiв на парковцi",
                /*7*/        "Створити/поставити Транспортний засiб на Парковку",
                /*8*/        "Видалити/забрати Транспортний засiб з Парковки",
                /*9*/        "Поповнити баланс конкретного Транспортного засобу"
            };

        public void ShowMainMenu()
        {
            int i = 1;
            foreach (string c in MainMenuContainer)
            {
                Console.WriteLine("  " + i++ + " - " + c + "\n");
            }
          
            Console.WriteLine();
        }

        public void ShowHeader()
        {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.DarkGray;           
            Console.WriteLine("     ******************************************");
            Console.Write("     *    Програма ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("<< Парковка бiля дому >>");
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine("   *");
            Console.WriteLine("     ******************************************");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Gray;
            
        }

        public void Execute_Task1(Parking p)          // Дiзнатися поточний баланс парковки
        {
            Console.Clear();
            ShowHeader();
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[0]}"); 
            Console.WriteLine();
            p.ShowParkingBalance();
            Console.ReadKey();
            
        }

        public void Execute_Task2()                         //   "Сума зароблених коштiв за останню хвилину"
        {
            Console.Clear();
            ShowHeader();
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[1]}");
            Console.WriteLine();

            DateTime End = DateTime.Now;
            TimeSpan duration = TimeSpan.FromMinutes(1);
            DateTime Start = End - duration;

            float sum_balance_parking = 0;
            foreach (KeyValuePair<DateTime,float> kvp in Parking.ParkingBalance)
            {
                if (kvp.Key > Start)
                {
                    sum_balance_parking += kvp.Value;
                }
            }
           
            Console.WriteLine($"Сума коштiв, зароблених парковкою за останню хвилину становить {sum_balance_parking} грошей");          
            Console.ReadKey();
        }

        public void Execute_Task3(Parking p)          //  Дiзнатися кiлькiсть вільних/зайнятих мiсць на парковцi
        { 
            Console.Clear();
            ShowHeader();
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[2]}");
            Console.WriteLine();
            p.ShowQuantityVehiclesParked();
            Console.ReadKey();
        }

        public void Execute_Task4(Parking parking)                         //  Вивести на екран всi Транзакцii Парковки за останню хвилину
        {
            Console.Clear();
            ShowHeader();
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[3]}");
            Console.WriteLine();

            DateTime End = DateTime.Now;
            TimeSpan duration = TimeSpan.FromMinutes(1);
            DateTime Start = End - duration;
           
            foreach (KeyValuePair<DateTime, PaymentDetails> kvp in Parking.Transactions)
            {
                if (kvp.Key > Start)
                {
                    Console.WriteLine(" {0}  vehicle_Id  {1}    amount_spent  {2}   vehicle_type {3}   balance {4} \n",
                        kvp.Key.ToLongTimeString(), kvp.Value.vehicleId, kvp.Value.amountspent, kvp.Value.vehicletype, kvp.Value.vehicle_bal);
                }
            }

            Console.ReadKey();
            
        }

        public void Execute_Task5()                         //  Вивести усю iсторiю Транзакцiй (зчитавши данi з файлу Transactions.log)
        {
            Console.Clear();
            ShowHeader();
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[4]}");
            Console.WriteLine();

            using (StreamReader r = File.OpenText("Transactions.log"))
            {
                DumpLog(r);
            }

            void DumpLog(StreamReader r)
            {
                string line;
                while ((line = r.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                }
            }

            Console.ReadKey();
        }

        public void Execute_Task6(Parking parking)                         // Вивести на екран список усiх Транспортних засобiв на парковцi
        { 
            Console.Clear();
            ShowHeader();
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[5]}");
            Console.WriteLine();
            parking.ShowQuantityVehiclesParked();
            Console.WriteLine("  Запарковано такi транспортнi засоби:");
            Console.WriteLine();
            parking.ShowListOfVehiclesParked();
            Console.ReadKey();
        }

        public void Execute_Task7(ref Parking parking)                         //  Створити/поставити Транспортний засiб на Парковку
        {
            Console.Clear();
            ShowHeader();
            int number_id = 0; //вказівник, який дозволяє уникнути транспортних засобів із однаковим номером
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[6]}");
            Console.WriteLine();
            Console.WriteLine("  Щоб створити транспортний засiб, спочатку введiть його номер (довiльно), \n  а потiм виберiть його тип: легковик, вантажiвка, автобус або мотоцикл \n");
            Console.Write("  Номер транспортного засобу:");
            string number = Console.ReadLine();
            if (number == "")
            {
                Console.WriteLine("  номер не введено! повернiться в головне меню");
                Console.ReadKey();
            }
            else
            {
                foreach (string c in parking.Parked_vehicles.Keys)
                {
                    if (number == c)
                    {
                        Console.WriteLine("  такий транспотний засiб вже запаркований, спробуйте ще");
                        number_id = 1;
                    }
                }
                if (number_id == 0)
                {
                    Console.WriteLine();
                    Console.WriteLine("  1 - Легковик / smallcar");
                    Console.WriteLine("  2 - Вантажiвка / lorry");
                    Console.WriteLine("  3 - Автобус / bus");
                    Console.WriteLine("  4 - Мотоцикл / bike");
                    string type_of_vehicle = Console.ReadLine();
                    switch (type_of_vehicle)
                    {
                        case "1":
                            {
                                Vehicle vehicle = new Vehicle(number, Vehicle.VehicleType.smallcar);
                                parking.Parked_vehicles[number] = vehicle;
                                parking.VehicleAdded += Log_Parking_Vehicle_Added;
                                parking.VehicleAdd();
                                parking.VehicleAdded -= Log_Parking_Vehicle_Added;
                                vehicle.Display_Info_about_vehicle();
                                break;
                            }
                        case "2":
                            {
                                Vehicle vehicle = new Vehicle(number, Vehicle.VehicleType.lorry);
                                parking.Parked_vehicles[number] = vehicle;
                                parking.VehicleAdded += Log_Parking_Vehicle_Added;
                                parking.VehicleAdd();
                                parking.VehicleAdded -= Log_Parking_Vehicle_Added;
                                vehicle.Display_Info_about_vehicle();
                                break;
                            }
                        case "3":
                            {
                                Vehicle vehicle = new Vehicle(number, Vehicle.VehicleType.bus);
                                parking.Parked_vehicles[number] = vehicle;
                                parking.VehicleAdded += Log_Parking_Vehicle_Added;
                                parking.VehicleAdd();
                                parking.VehicleAdded -= Log_Parking_Vehicle_Added;
                                vehicle.Display_Info_about_vehicle();
                                break;
                            }
                        case "4":
                            {
                                Vehicle vehicle = new Vehicle(number, Vehicle.VehicleType.bike);
                                parking.Parked_vehicles[number] = vehicle;
                                parking.VehicleAdded += Log_Parking_Vehicle_Added;
                                parking.VehicleAdd();
                                parking.VehicleAdded -= Log_Parking_Vehicle_Added;
                                vehicle.Display_Info_about_vehicle();

                                break;
                            }
                        default:
                            {
                                Console.WriteLine("  Такого типу авто Вам не було запропоновано в списку");
                                break;
                            }
                    }

                    Console.ReadKey();
                }
                else
                {
                    Console.ReadKey();
                    number_id = 0;
                }
            }
        }
      
        public void Execute_Task8(ref Parking parking)                         //  Видалити/забрати Транспортний засiб з Парковки
        {
            Console.Clear();
            ShowHeader();
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[7]}");
            Console.WriteLine();
            Console.WriteLine("  На парковцi запаркованi такi транспортнi засоби:");
            Console.WriteLine();
            parking.ShowListOfVehiclesParked();
            Console.WriteLine();
            
            if (parking.vehicles_parked >= 1)
            {
                Console.Write("  Введiть номер ТЗ для видалення (з урахуванням регiстру лiтер): ");
                string number = Console.ReadLine();
                if (number == "")
                {
                    Console.WriteLine("  номер не введено");
                }
                else
                {
                    try
                    {
                        parking.Parked_vehicles.Remove(number);
                        parking.VehicleRemoved += Log_Parking_Vehicle_Removed;
                        parking.VehicleRemove();
                        parking.VehicleRemoved -= Log_Parking_Vehicle_Removed;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine("  Транспортний засiб з таким номером відсутнiй");
                    }
                }
            }
            else
            {
                Console.WriteLine(" Спочатку поставте транспортний засiб на парковку");
            }


            if (parking.vehicles_parked >= 1)
            {
                Console.WriteLine("  Залишились на парковцi такi транспортнi засоби:");
                Console.WriteLine();
                parking.ShowListOfVehiclesParked();
            }
            Console.ReadKey();
        }

        public void Execute_Task9(ref Parking parking)                         //  Поповнити баланс конкретного Транспортного засобу
        { 
            Console.Clear();
            ShowHeader();
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[8]}");
            Console.WriteLine();
            Console.WriteLine("  На парковцi запаркованi такi транспортнi засоби:");
            Console.WriteLine();
            parking.ShowListOfVehiclesParked();
            Console.WriteLine();
            if (parking.vehicles_parked >= 1)
            {
                Console.Write("  Введiть державний номер транспортного засобу, для якого хочете поповнити рахунок: ");
                string tz_number = Console.ReadLine();
                Console.WriteLine();
                Console.Write($"  На яку суму хочете поповнити рахунок для даного транспортного засобу? введiть суму:");
                try
                {
                    float sum = Convert.ToInt32(Console.ReadLine());
                    try
                    {
                        Vehicle modified_balance = parking.Parked_vehicles[tz_number];
                        modified_balance.ToppedUp += Log_TopUp;
                        modified_balance.TopUp(sum);
                        modified_balance.ToppedUp -= Log_TopUp;
                        parking.Parked_vehicles[tz_number] = modified_balance;

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine("  Транспортний засiб з таким номером відсутнiй");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(" грошей ми не побачили, спробуйте ще");
                }
               
               
            }
            else {
                Console.WriteLine(" Спочатку поставте транспортний засiб на парковку");
            }
                                 
            Console.ReadKey();
        }

        private static void Log_TopUp(object sender, BalanceVehicleEventArgs e) //обробник події додавання коштів на баланс транспортного засобу
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"  Ви вибрали поповнити рахунок транспортного засобу на {e.Amount} грошей");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(e.ActionMessage + "\n");

        }

        public static void Log_Parking_Vehicle_Added(object sender, BalanceParkingEventArgs e) //обробник події додавання транспортного засобу на парковку
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"  Зараз буде додано транспортний засiб на парковку");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(e.ActionMessage + "\n");

        }

        private static void Log_Parking_Vehicle_Removed(object sender, BalanceParkingEventArgs e) //обробник події прибирання транспортного засобу з парковки
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"  Зараз буде прибрано транспортний засiб з парковки");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(e.ActionMessage + "\n");
        }

        public void ExecuteMainMenu(ref Parking parking)
        {
            string n = "o";

            while (n != "q")
            {
                ShowHeader();
                ShowMainMenu();
                Console.WriteLine("  Виберiть необхiдний пункт меню або натиснiть q для виходу");
                n = Convert.ToString(Console.ReadLine());
                switch (n)
                {
                    case "1":
                        {
                            Execute_Task1(parking);                          
                            break;
                        }
                    case "2":
                        {
                            Execute_Task2();
                            break;
                        }
                    case "3":
                        {
                            Execute_Task3(parking);
                            break;
                        }
                    case "4":
                        {
                            Execute_Task4(parking);
                            break;
                        }
                    case "5":
                        {
                            Execute_Task5();
                            break;
                        }
                    case "6":
                        {
                            Execute_Task6(parking);
                            break;
                        }
                    case "7":
                        {
                            Execute_Task7(ref parking);
                            break;
                        }
                    case "8":
                        {
                            Execute_Task8(ref parking);
                            break;
                        }
                    case "9":
                        {
                            Execute_Task9(ref parking);
                            break;
                        }
                    default:
                        {
                            
                            break;
                        }

                }
                Console.Clear();
            }
        }
    }

    class MyTimers
    {
        private static Timer aTimer;

        public MyTimers(int N)
        {
            SetTimer(N);

        }

        public static void SetTimer(int N) //таймер, який щохвилини записує в файл
        {
            // Create a timer with a two second interval.
            aTimer = new System.Timers.Timer(N);
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        public static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {

        }

        public void Stoptimer()
        {
            aTimer.Stop();
            aTimer.Dispose();
        }

    }

    class Program
    {
        private static System.Timers.Timer aTimer;
 

        static void Main(string[] args)
        {
            Parking parking = new Parking();
            SetTimerToFile();
            
            parking.SetParkingBalance();           
            MainMenu mainmenu = new MainMenu();           
            mainmenu.ExecuteMainMenu(ref parking);
            aTimer.Stop();
            aTimer.Dispose();
     
            void SetTimerToFile() //таймер, який щохвилини записує в файл
            {
                // Create a timer with a two second interval.
                aTimer = new System.Timers.Timer(10000);
                // Hook up the Elapsed event for the timer. 
                aTimer.Elapsed += OnTimedEventToFile;
                aTimer.AutoReset = true;
                aTimer.Enabled = true;
            }

            void OnTimedEventToFile(Object source, ElapsedEventArgs e)
            {
                using (StreamWriter w = File.AppendText("Transactions.log"))
                {
                    //Log("Test1", w);
                    Log(w);
                }

            }
           
        }
                       
        public static void Log(TextWriter w)
        {
            DateTime End = DateTime.Now;
            TimeSpan duration = TimeSpan.FromMinutes(1);
            DateTime Start = End - duration;

            foreach (KeyValuePair<DateTime, PaymentDetails> kvp in Parking.Transactions)
            {
                if (kvp.Key > Start)
                {

                    w.WriteLine(" {0}  vehicle_Id  {1}    amount_spent  {2}   vehicle_type {3}   balance {4} \n",
                        kvp.Key.ToLongTimeString(), kvp.Value.vehicleId, kvp.Value.amountspent, kvp.Value.vehicletype, kvp.Value.vehicle_bal);
                }
            }
                     
        }

       
    }

}
